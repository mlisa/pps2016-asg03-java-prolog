% symbol(+OnBoard,-OnScreen): symbols for the board and their rendering
symbol(null,'_').
symbol(p1,'X').
symbol(p2,'O').

% result(+OnBoard,-OnScreen): result of a game
result(even,"even!").
result(p1,"player 1 wins").
result(p2,"player 2 wins").

% other_player(?Player,?OtherPlayer)
other_player(p1,p2).
other_player(p2,p1).

% render(+List): prints a TTT table (9 elements list) on console
render(L) :- convert_symbols(L,[A,B,C,D,E,F,G,H,I,L,M,N,O,P,Q,R]),print_row(A,B,C,D),print_row(E,F,G,H),print_row(I,L,M,N),print(O,P,Q,R).
convert_symbols(L,L2) :- findall(R,(member(X,L),symbol(X,R)),L2).
print_row(A,B,C,D) :- put(A),put(' '),put(B),put(' '),put(C), put(' '), put(D),nl.

% render(+List,+Result): prints a TTT table plus result
render_full(L,Result) :- result(Result,OnScreen),print(OnScreen),nl,render(L),nl,nl.

% create_board(-Board): creates an initially empty board
create_board(B):-create_list(16,null,B).
create_list(0,_,[]) :- !.
create_list(N,X,[X|T]) :- N2 is N-1, create_list(N2,X,T).

% next_board(+Board,+Player,?NewBoard): finds (zero, one or many) new boards as Player moves
next_board([null|B],PL,[PL|B]).
next_board([X|B],PL,[X|B2]):-next_board(B,PL,B2).

% final(+Board,-Result): checks where the board is final and why
%final(B,p1) :- finalpatt(P), match(B,P,p1),!.
%final(B,p2) :- finalpatt(P), match(B,P,p2),!.
final(Board,Player) :- split_four(Board, A,B,C,D), match_final(A,B,C,D, Player).
final(Board,Player) :- split_four(Board, A,B,C,D), match_final(A,B,C,D, Player).
final(Board,even) :- not(member(null,Board)).

%split(+List, -FirstPart, -SecondPart): divides a list in two equal parts
split(L, A, B) :-
    append(A, B, L),
    length(A, N),
    length(B, N).

%split_four(+List, -FirstPart, -SecondPart, -ThirdPart, -FourthPart): divides a list in four equal parts
split_four(L,A,B,C,D) :- split(L, X, Y),split(X, A, B),split(Y, C, D).

%second(+L, -X): returns second element of the given list
second([_,X|_], X).
%third(+L, -X): returns third element of the given list
third([_,_,X|_], X).
%fourth(+L, -X): returns fourth element of the given list
fourth([_,_,_,X|_], X).

%check_col(+L1, +L2, +L3, +L4, +Player): check if the Player completed a column
check_col([Player|T1],[Player|T2],[Player|T3],[Player|T4], Player).
check_col([H1|T1],[H2|T2],[H3|T3],[H4|T4], Player) :- check_col(T1,T2,T3,T4, Player).
%check_row_elem(+Player, +L): check if the List L has all the same elements Player
check_row_elem(_, []).
check_row_elem(Player, [Player|T]) :- check_row_elem(Player, T).
%check_row(+L, +Player): check if the Player completed a row
check_row([Player|T], Player) :- check_row_elem(Player,T).

%match_final(+A,+B,+C,+D, +Player): having the four rows A,B,C,D checks if Player won.
match_final(A,B,C,D, Player) :- check_col(A,B,C,D,Player).
match_final(A,B,C,D, Player) :- check_row(A, Player).
match_final(A,B,C,D, Player) :- check_row(B, Player).
match_final(A,B,C,D, Player) :- check_row(C, Player).
match_final(A,B,C,D, Player) :- check_row(D, Player).
match_final([Player|T],B,C,D, Player) :- second(B,Player), third(C,Player), fourth(D,Player).
match_final(A,B,C,[Player|T], Player) :- fourth(A, Player), third(B,Player), second(C,Player).

% game(+Board,+Player,-FinalBoard,-Result): finds one (zero, one or many) final boards and results
game(B,_,B,Result) :- final(B,Result),!.
game(B,PL,BF,Result):- next_board(B,PL,B2), other_player(PL,PL2),game(B2,PL2,BF,Result).

% statistics(+Board,+Player,+Result,-Count): counts how many time Res will happen
statistics(B,P,Res,Count) :- findall(a, game(B,P,_,Res),L), length(L,Count).




