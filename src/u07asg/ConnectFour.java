package u07asg;

import java.util.List;
import java.util.Optional;

enum Player { PlayerX, PlayerO }

public interface ConnectFour {

    void createBoard();

    List<Optional<Player>> getBoard();

    boolean checkCompleted();

    Optional<Player> checkVictory();

    boolean move(Player player, int i, int j);

    int winCount(Player current, Player winner);
}
