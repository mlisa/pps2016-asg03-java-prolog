package u07asg;


import javax.swing.*;
import java.awt.*;
import java.util.Optional;

/** A not so nicely engineered App to run TTT games, not very important.
 * It just works..
 */
public class ConnectFourApp {

    private final ConnectFour connectFour;
    private final JButton[][] board = new JButton[4][4];
    private final JButton exit = new JButton("Exit");
    private final JFrame frame = new JFrame("ConnectFour");
    private boolean finished = false;
    private Player turn = Player.PlayerX;
    private int moves = 0;

    private void changeTurn(){
        this.turn = this.turn == Player.PlayerX ? Player.PlayerO : Player.PlayerX;
    }

    public ConnectFourApp(ConnectFour ttt) throws Exception {
        this.connectFour =ttt;
        initPane();
    }
    
    private void humanMove(int i, int j){
        if (connectFour.move(turn,i,j)){
            board[i][j].setText(turn == Player.PlayerX ? "X" : "O");
            if(i!=0) {
                board[i - 1][j].setEnabled(true);
            }
            moves++;
            changeTurn();
            if (moves > 10){
                System.out.println("X winning count: "+ connectFour.winCount(turn, Player.PlayerX));
                System.out.println("O winning count: "+ connectFour.winCount(turn, Player.PlayerO));
            }
        }
        Optional<Player> victory = connectFour.checkVictory();
        if (victory.isPresent()){
            exit.setText(victory.get()+" won!");
            finished=true;
            return;
        }
        if (connectFour.checkCompleted()){
            exit.setText("Even!");
            finished=true;
            return;
        }
    }

    private void initPane(){
        frame.setLayout(new BorderLayout());
        JPanel b=new JPanel(new GridLayout(4,4));
        for (int i=0;i<4;i++){
            for (int j=0;j<4;j++){
                final int i2 = i;
                final int j2 = j;
                JButton button = new JButton("");
                button.setEnabled(i==3);
                board[i][j]= button;
                b.add(board[i][j]);
                board[i][j].addActionListener(e -> { if (!finished) humanMove(i2,j2); });
            }
        }
        JPanel s=new JPanel(new FlowLayout());
        s.add(exit);
        exit.addActionListener(e -> System.exit(0));
        frame.add(BorderLayout.CENTER,b);
        frame.add(BorderLayout.SOUTH,s);
        frame.setSize(200,230);
        frame.setVisible(true);
    }

    public static void main(String[] args){
        try {
            new ConnectFourApp(new ConnectFourImpl("src/u07asg/connectFour.pl"));
        } catch (Exception e) {
            System.out.println("Problems loading the theory");
        }
    }
}
